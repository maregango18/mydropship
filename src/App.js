import React, { useEffect, useState } from "react";
import Main from "./Components/Main";
import "./App.css";
import Sidebar from "./Components/Sidebar";
import Aside from "./Components/aside/Aside";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  Redirect,
} from "react-router-dom";
import axios from "axios";
import Login from "./Components/Login";
import Cart from "./Components/Cart";
import Catalog from "./Components/Catalog";
import Product from "./Components/Product";
import { getProduct } from "./Components/Product";
import LogIn from "./Components/Mare/LogIn";
import Navbar from "./Pages/Homepage/Navbar";
import Section from "./Pages/Homepage/Section";
import Home from "./Pages/Homepage/Homepage";
import Registration from "./Components/Registration";
import SignUp from "./Components/Mare/SignUp";
import Profile from "./Pages/Profile";
import Dashboard from "./Pages/Dashboard";
import Inventory from "./Pages/Inventory";
import Orders from "./Pages/Orders";
import Transactions from "./Pages/Transactions";
import Store from "./Pages/Store";
import ProductListing from "./containers/ProductListing";
import ProductDetails from "./containers/ProductDetails";
import SuccessSnackbar from "./containers/SuccessSnackbar";

export function App() {
  return (
    <div className="App">
      <SuccessSnackbar />
      <Router>
        {/* <Sidebar/> */}
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/login">
            <LogIn />
          </Route>
          <Route path="/signup">
            <SignUp />
          </Route>
          <Route path="/Profile">
            <Profile />
          </Route>
          <Route path="/Dashboard">
            <Dashboard />
          </Route>
          <Route path="/Catalog">
            <ProductListing />
          </Route>
          {/* <Route path="/product/:productId" component={ProductDetails} /> */}
          <Route path="/Inventory">
            <Inventory />
          </Route>
          <Route path="/Cart">
            <Cart />
          </Route>
          <Route path="/Orders">
            <Orders />
          </Route>
          <Route path="/Transactions">
            <Transactions />
          </Route>
          <Route path="/Store">
            <Store />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
