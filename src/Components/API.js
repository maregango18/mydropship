import axios from "axios";

const SERVER_URL = "http://18.185.148.165:3000/";
const SERVER_URL_V1 = SERVER_URL + "api/v1/";

axios.interceptors.request.use((config) => {
  config.headers.authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export const registration = async (
  firstName,
  lastName,
  email,
  password,
  passwordConfirmation
) => {
  console.log("miha wadi");
  try {
    const result = await axios.post(SERVER_URL + "register", {
      firstName,
      lastName,
      email,
      password,
      passwordConfirmation,
    });
    localStorage.setItem("user", JSON.stringify(result.data.data));
    localStorage.setItem("token", JSON.stringify(result.data.data.token));
    console.log(result);
  } catch (err) {
    alert("MIHA");
  }
};

export const login = async (data) => {
  const results = await axios.post(SERVER_URL + "login", {
    password: data.password,
    email: data.email,
  });
  console.log(results.data.data);
  localStorage.setItem("user", JSON.stringify(results.data.data));
  localStorage.setItem("token", results.data.data.token);
};

export const cart = async () => {
  const results = await axios.get(SERVER_URL_V1 + "cart");
  return results.data.data;
  // try {
  //     const results = await axios.get(SERVER_URL_V1 + 'cart');
  //     return results.data.data;
  // } catch(err){
  //     if (err.response.status === 401){
  //         localStorage.removeItem('token');
  //         localStorage.removeItem('user');
  //         window.location.href = '/login';
  //     }
  // }
};

export const getProducts = async () => {
  const results = await axios.get(SERVER_URL_V1 + "products");
  return results.data.data;
};

// export const addToCart = async (productId , qty)=> {
//     const results = await axios.get(SERVER_URL_V1 + 'cart/add',{productId,qty});
//     return results.data.data;
// };
export const addToCart = async (productId, qty) => {
  const result = await axios.post(SERVER_URL_V1 + "cart/add", {
    productId,
    qty,
  });
  return result.data.data;
};

// export const addProduct = async (data) => {
//     const results = await axios.post(SERVER_URL_V1 + 'products',data);
//     return results.data.data;
// };

// export const updateProduct = async (id,data) => {
//     const results = await axios.put(SERVER_URL_V1 + `products/${id}`,data);
//     return results.data.data;
// };

export const removeCartItem = async (itemId) => {
  const response = await axios.post(
    `${SERVER_URL}api/v1/cart/remove/${itemId}`
  );
  return response;
};

export default login;
