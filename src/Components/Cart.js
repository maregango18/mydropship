import React from "react";
import { useEffect, useState } from "react";
import ProductComponent from "../containers/ProductComponent";
import { cart } from "./API";
// import Logout from "./Logout";
import Sidebar from "./Sidebar";
import SingleProduct from "./SingleProduct";
import "./cart.css";

const Cart = () => {
  console.log("cart");
  const [cartData, setCartData] = useState({});
  const [itemQuantity, setItemQuantity] = useState(0);

  useEffect(() => {
    cart().then((result) => {
      console.log("res", result);
      setCartData(result);
    });
  }, [itemQuantity]);

  console.log(cartData?.cartItem);
  return (
    <>
      <Sidebar />
      <div className="cart__wrapper">
        <h4 className="cart__title"> Shopping Cart ()</h4>
        <div className="cart__data">
          <span className="cart_details">Item Description</span>
          <span className="cart_details">Quantity</span>
          <span className="cart_details">Delete</span>
        </div>
        {cartData?.cartItem?.items?.map((item) => {
          console.log(item);
          return (
            <SingleProduct
              id={item.id}
              title={item.title}
              image={item.image}
              qty={item.qty}
              key={item.id}
              setItemQuantity={setItemQuantity}
            />
          );
        })}
      </div>
    </>
  );
};

export default Cart;
