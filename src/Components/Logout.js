import {useHistory} from 'react-router-dom';


const Logout = () => {

    const history = useHistory();

    const performLogout = () => {
        localStorage.clear();
        history.push('/login');
    };
    return (
        <input type="button" value="Logout" onClick={performLogout}/>
    );
};

export default Logout;