import React, { useEffect, useState } from 'react';
import Header from './main/Header/Header';
import './Main.css';
import Navbar from './main/navbar/Navbar';
import Catalog from './main/catalog/Catalog';
{/* <Router>
  
  <div>
    <ul>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/about">About</Link>
      </li>
      <li>
        <Link to="/topics">Topics</Link>
      </li>
    </ul>

    <Switch>
      <Route path="/about">
        <About />
      </Route>
      <Route path="/topics">
        <Topics />
      </Route>
      <Route path="/">
        <Home />
      </Route>
    </Switch>
  </div>
</Router>
);
}

function Home() {
return <h2>Home</h2>;
}

function About() {
return <h2>About</h2>;
}

function Topics() {
let match = useRouteMatch();

return (
<div>
  <h2>Topics</h2>

  <ul>
    <li>
      <Link to={`${match.url}/components`}>Components</Link>
    </li>
    <li>
      <Link to={`${match.url}/props-v-state`}>
        Props v. State
      </Link>
    </li>
  </ul>

  
  <Switch>
    <Route path={`${match.path}/:topicId`}>
      <Topic />
    </Route>
    <Route path={match.path}>
      <h3>Please select a topic.</h3>
    </Route>
  </Switch>
</div>
);
}

function Topic() {
let { topicId } = useParams();
return <h3>Requested topic ID: {topicId}</h3>;
} */}




const Main = () => {

    return (
        <div className="Main">
            <Header />
            <Navbar />
            <Catalog />
        </div>
    );
}

export default Main;
