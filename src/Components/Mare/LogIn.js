import { useFormik } from "formik";
import * as Yup from "yup";
import { TextField, Button } from "@material-ui/core";
import { login } from "../API";
import { InputAdornment } from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookF } from "@fortawesome/free-brands-svg-icons";
import { faLinkedinIn } from "@fortawesome/free-brands-svg-icons";
import { makeStyles } from "@material-ui/core/styles";
import PersonIcon from "@material-ui/icons/Person";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import "./SignUp.css";
import { useHistory } from "react-router";
import { useEffect } from "react";
import { Link } from "react-router-dom";

const styled = {
  color: "#61D5DF",
  backgroundColor: "#61D5DF",
};

const LoginLogo = "https://app.365dropship.com/assets/images/dropship_logo.png";
const validationSchema = Yup.object({
  email: Yup.string()
    .email("Invalid email format")
    .required("Email is Required"),
  password: Yup.string()
    .min(4, "Password minimum length should be 8")
    .required("Password is Required"),
});
const LogIn = () => {
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: () => {
      console.log(formik.values);
      login(formik.values).then(history.push("/catalog"));
    },
    validationSchema: validationSchema,
  });

  const token = localStorage.getItem("token");
  console.log(token);

  useEffect(() => {
    if (token) {
      history.push("/catalog");
    }
  }, []);

  return (
    <div className="formbackground">
      <div className="form_wrapper">
        <form className="loginform" onSubmit={formik.handleSubmit}>
          <div className="memberslog">
            <img className="loginlogo" src={LoginLogo} alt={"LoginLogo"} />
            <span className="memberslogin">Members Log In</span>
          </div>
          <TextField
            id="email"
            name="email"
            label="Email Address"
            margin="normal"
            variant="outlined"
            style={{ width: "80%", borderColor: "#61D5DF", color: "#61D5DF" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <MailOutlineIcon color="primary" />
                </InputAdornment>
              ),
            }}
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <TextField
            id="password"
            name="password"
            label="Password"
            margin="normal"
            variant="outlined"
            type="password"
            style={{ width: "80%", color: "#61D5DF" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <VpnKeyIcon color="primary" />
                </InputAdornment>
              ),
            }}
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <span className="log">Forgot password?</span>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            onClick={onsubmit}
            style={{ width: "80%", backgroundColor: "#61D5DF" }}
          >
            Log In{" "}
          </Button>
          <span className="log"> Or Log In With</span>
          <div className="social__icons">
            <FontAwesomeIcon style={{ color: "#61D5DF" }} icon={faLinkedinIn} />
            <FontAwesomeIcon style={{ color: "#61D5DF" }} icon={faFacebookF} />
          </div>
          <div className="account__login">
            <span className="log">Don't have an account?</span>
            <Link
              to="/signup"
              style={{ textDecoration: "none", color: "#61D5DF" }}
            >
              <span>Sign Up</span>
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default LogIn;

// import React from 'react'
// import { Grid,Paper, Avatar, TextField, Button, Typography,Link } from '@material-ui/core'
// import {Formik, Form} from 'formik';
// import * as Yup from 'yup';
// import FormikControl from './FormikControl';

// const LogIn=(Login)=>{

//     const initialValues = {
//         email: '',
//         password: ''
//     }

//     const validationSchema = Yup.object({
//         email:Yup.string().email('Invalid email format').required('Required'),
//         password: Yup.string().required('Required')
//     })

//     const onSubmit = values => {
//         login('Form data', values)
//     }
//     return(
//         <Formik initialValues={initialValues} validationSchema= {validationSchema} onSubmit= {onSubmit}>
//             {
//                 formik => {
//                     return <Form>
//                         <FormikControl
//                         control='input'
//                         type='email'
//                         label='email'
//                         name ='email'/>
//                         <FormikControl
//                         control='input'
//                         type='password'
//                         label='password'
//                         name ='password'/>
//                         <button type = 'submit' disabled = {!formik.isValid}>Log In</button>
//                     </Form>
//                 }
//             }
//         </Formik>
//     )
// }

// export default LogIn;
