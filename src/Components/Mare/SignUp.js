import { useFormik } from "formik";
import * as Yup from "yup";
import { TextField, Button } from "@material-ui/core";
import { registration } from "../API";
import { InputAdornment } from "@material-ui/core";
import PersonIcon from "@material-ui/icons/Person";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import "./SignUp.css";

const LoginLogo = "https://app.365dropship.com/assets/images/dropship_logo.png";

const validationSchema = Yup.object({
  firstName: Yup.string().min(3, "It's too short").required("Required"),
  lastName: Yup.string().min(3, "It's too short").required("Required"),
  email: Yup.string().email("Enter valid email").required("Required"),
  password: Yup.string()
    .min(4, "Password minimum length should be 8")
    .required("Required"),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref("password")], "Password not matched")
    .required("Required"),
});
const SignUp = () => {
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      passwordConfirmation: "",
    },
    onSubmit: (values) => {
      console.log(values);
      registration(values);
    },
    validationSchema: validationSchema,
  });

  return (
    <div className="formbackground">
      <div className="signup_wrapper">
        <form className="signupform" onSubmit={formik.handleSubmit}>
          <div className="memberslog">
            <img className="loginlogo" src={LoginLogo} alt={"LoginLogo"} />
            <span className="memberslogin">Members Log In</span>
          </div>
          <TextField
            id="firstName"
            name="firstName"
            label="FirstName"
            margin="normal"
            variant="outlined"
            style={{ width: "80%", color: "aquablue" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PersonIcon color="primary" />
                </InputAdornment>
              ),
            }}
            value={formik.values.firstName}
            onChange={formik.handleChange}
            error={formik.touched.firstName && Boolean(formik.errors.firstName)}
            helperText={formik.touched.firstName && formik.errors.firstName}
          />
          <TextField
            id="lastName"
            name="lastName"
            label="LastName"
            margin="normal"
            variant="outlined"
            style={{ width: "80%", color: "aquablue" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PersonIcon color="primary" />
                </InputAdornment>
              ),
            }}
            value={formik.values.lastName}
            onChange={formik.handleChange}
            error={formik.touched.lastName && Boolean(formik.errors.lastName)}
            helperText={formik.touched.lastName && formik.errors.lastName}
          />
          <TextField
            id="email"
            name="email"
            label="Email Address"
            margin="normal"
            variant="outlined"
            style={{ width: "80%", color: "aquablue" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <MailOutlineIcon color="primary" />
                </InputAdornment>
              ),
            }}
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <TextField
            id="password"
            name="password"
            label="Password"
            margin="normal"
            variant="outlined"
            type="password"
            style={{ width: "80%", color: "aquablue" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <VpnKeyIcon color="primary" />
                </InputAdornment>
              ),
            }}
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
          />
          <TextField
            id="passwordConfirmation"
            name="passwordConfirmation"
            label="Confirm Password"
            margin="normal"
            variant="outlined"
            type="password"
            style={{ width: "80%", color: "aquablue" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <VpnKeyIcon color="primary" />
                </InputAdornment>
              ),
            }}
            value={formik.values.passwordConfirmation}
            onChange={formik.handleChange}
            error={
              formik.touched.passwordConfirmation &&
              Boolean(formik.errors.passwordConfirmation)
            }
            helperText={
              formik.touched.passwordConfirmation &&
              formik.errors.passwordConfirmation
            }
          />
          <Button
            type="submit"
            variant="outlined"
            style={{ width: "80%", backgroundColor: "#61D5DF" }}
          >
            Sign Up{" "}
          </Button>
        </form>
      </div>
    </div>
  );
};

export default SignUp;
