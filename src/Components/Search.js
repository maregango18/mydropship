import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ProductSearchAction } from '../redux/actions/productActions';
import './Search.css';

const Search = () => {
    const dispatch = useDispatch();
    const query = useSelector(state => state.products.search);
    const changeInputText = (e) => {
        dispatch(ProductSearchAction(e.target.value))
    }

    return (
        <div classNameName={"search__content"}>
            <input
                id={"search"}
                className={"search__input"}
                type={"text"}
                placeholder={"Search"}
                value={query} onChange={changeInputText}
            />
            <button
                id={"search__button"}
                className={"header__searchbtn"}
                type={"submit"}
            >
            <i className="fas fa-search fa-rotate-90"></i>
            </button>
        </div>
    );
};
export default Search;


