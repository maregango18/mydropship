import dashboard from "./assets/dashboard.svg";
import cart from "./assets/cart.svg";
import catalog from "./assets/catalog.svg";
import inventory from "./assets/inventory.svg";
import orders from "./assets/orders.svg";
import transactions from "./assets/transactions.svg";
import storesList from "./assets/storesList.svg";
import profile from "./assets/profile.jpg";
import dropship_logo from "./assets/dropship_logo.png";
import "./Sidebar.css";
import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <div className={"nav"} id={"navbar"}>
      <nav className={"nav__container"}>
        <div>
          <Link exact to="/">
            <img
              src={dropship_logo}
              className={"dropshipLogo"}
              alt={"365Dropship Logo"}
            />
          </Link>
          <div className={"nav__list"}>
            <div className={"nav__items"}>
              <Link to="/profile">
                <img src={profile} className={"profile"} />
              </Link>
              <Link to="/catalog">
                <img src={catalog} className={"red"} />
              </Link>

              <Link to="/dashboard">
                <img src={dashboard} />
              </Link>

              <Link to="/inventory">
                <img src={inventory} />
              </Link>
              <Link to="/cart">
                <img src={cart} />
              </Link>
              <Link to="/orders">
                <img src={orders} />
              </Link>
              <Link to="/transactions">
                <img src={transactions} />
              </Link>
              <Link to="/store">
                <img src={storesList} />
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Sidebar;
