import { addToCart } from "./API";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import "./SingleProduct.css";
import { removeCartItem } from "../Components/API";

const SingleProduct = ({ id, image, title, qty, setItemQuantity }) => {
  const removeButtonHandler = () => {
    removeCartItem(id);
    setItemQuantity((prev) => prev + 1);
  };

  const minusClickHandler = () => {
    addToCart(id, -1);
    setItemQuantity((prev) => prev + 1);
  };
  const plusClickHandler = () => {
    addToCart(id, 1);
    setItemQuantity((prev) => prev + 1);
  };

  return (
    <div
      className="cart__container"
      key={id}
      style={{ border: "1px solid black" }}
    >
      <img className="cart__img" src={image} />
      <p className="cart__title">{title}</p>

      <button className="qnt__btn" onClick={minusClickHandler}>
        -
      </button>
      <div className="quantity-box">
        <input type="text" value={qty}></input>
      </div>
      <button className="qnt__btn" onClick={plusClickHandler}>
        +
      </button>

      <button
        className="qnt__btn"
        style={{ marginLeft: 35 }}
        onClick={removeButtonHandler}
      >
        Delete
      </button>

      {/* <input
        type="button"
        value="Add to Cart"
        onClick={() => {
          addToCart(id, 1);
        }}
      /> */}
    </div>
  );
};

export default SingleProduct;
