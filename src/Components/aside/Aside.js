import React, { useEffect, useState } from "react";
// import Button from '../header/Button';
import { Route, Link, Switch, useHistory } from "react-router-dom";
import "./Aside.css";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

function Aside({ setCategories }) {
  const categoryHandler = (e) => {
    setCategories(e.target.value);
  };

  const history = useHistory();

  return (
    <div class="sidenav">
      <select className="sorting__dark">
        <option value="de">Choose Niche</option>
        <option value="de">Best Sellers</option>
        <option value="de">Beauty</option>
        <option value="de">Electronics</option>
        <option value="de">Fashion</option>
        <option value="de">Fragrances</option>
      </select>

      <select className="sorting__light" onChange={categoryHandler}>
        <option value="products">Choose Category</option>
        <option className="as" value="electronics">
          Electronics
        </option>
        <option className="as" value="jewelery">
          Jewelery
        </option>
        <option className="as" value="men's clothing">
          Men's Clothing
        </option>
        <option className="as" value="women's clothing">
          Women's Clothing
        </option>
      </select>
      <div className="shipping">
        <select className="sorting">
          <option value="de">Ship From</option>
          <option value="de">Best Sellers</option>
          <option value="de">Fashion</option>
        </select>
        <select className="sorting">
          <option value="de">Ship To</option>
          <option value="de">Best Sellers</option>
          <option value="de">Fashion</option>
        </select>
        <select className="sorting">
          <option value="de">Select Supplier</option>
          <option value="de">Best Sellers</option>
          <option value="de">Fashion</option>
        </select>
      </div>
      <div className="ranges">
        <p>Price Range </p>
        <p>Profit Range</p>
      </div>
      {/* <div class="button">
                <button class="btn btn--modal">Reset Filter</button>
                
            </div> */}
      <Button
        className="reset"
        variant="contained"
        color="primary"
        style={{
          backgroundColor: "#61d5df",
          width: "200px",
          color: "#fff",
        }}
      >
        {" "}
        Reset Filter
      </Button>
    </div>
  );
}
export default Aside;
