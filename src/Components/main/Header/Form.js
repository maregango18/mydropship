import React from 'react';
import Input from './Input';
import Button from './Button'
import './Form.css';

function Form(props) {
    return (
        <div className="Form">
            <Input />
            <Button name="Search" />
        </div>
    );
}
export default Form;