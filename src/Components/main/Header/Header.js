import React from 'react';
import './Header.css';
import Button from './Button';
import NumberOfProducts from './NumberOfProducts';
import Form from './Form';

function Header(props) {
    return (
        <div className="Header">
            <Button name="Select All" />
            <NumberOfProducts />
            <Form />
            <Button name="Add To Inventory" />
        </div>
    );
}

export default Header;