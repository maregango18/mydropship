import React, { useState } from 'react';
import './Input.css';
//import Main from '../Main';



function Input() {

    const [search, setSearch] = useState("");
    const [query, setQuery] = useState('chicken');

    const updateSearch = e => {
        setSearch(e.target.value);
        console.log(search);
    };

    const getSearch = e => {
        e.preventDefault();
        setQuery(search);
    }
    return (
        <input type="text" id="searchquery" placeholder="Search..." value={search} onchange={updateSearch} />
    );
}

export default Input;