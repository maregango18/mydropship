import React, { useEffect, useState } from 'react';
import Product from './Product';
import './Product.css';
import './Catalog.css';
import axios from 'axios';

const Catalog = () => {

    const [products, setProducts] = useState([]);


    useEffect(() => {
        axios
            .get("https://fakestoreapi.com/products")
            .then(res => {
                localStorage.setItem('products',JSON.stringify(res.data));
            })
    }, []);

    useEffect(()=> {
        const getProducts = JSON.parse(localStorage.getItem('products'));
        setProducts(getProducts);
    },[]);

    // const getProducts = async () => {
    //     const response = await fetch(
    //         "https://fakestoreapi.com/products"
    //     );
    //     const data = await response.json();
    //     setProducts(data);
    //     console.log(data);
    // };


    return (
        <div className="wrapper">
            <div className="catalog">
                {products.map(product => (
                    <Product
                        title={product.title}
                        image={product.image}
                        price={product.price}
                    />
                ))}
            </div>
        </div>
    );
}

export default Catalog;