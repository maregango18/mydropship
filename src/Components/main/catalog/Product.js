import React from 'react';
import './Product.css';

const Product = ({ title, image, price }) => {
    return (
        <div className="catalog__product">
            <div className="catalog__title"> {title}</div>
            <div class="catalog__photo">
                <img src={image} alt=" " />
            </div>
            <div class="catalog__prices"> {price}$ </div>
        </div>
    );
}

export default Product;