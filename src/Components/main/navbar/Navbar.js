import React from 'react';
import './Navbar.css';

function Navbar() {
    return (
        <nav class="nav">
            <div class="nav__sort">
                <select id="sort">
                    <option value="asc">Ascending</option>
                    <option value="desc">Descending</option>
                </select>
            </div>
        </nav>
    );
}

export default Navbar;