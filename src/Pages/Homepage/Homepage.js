import "./Homepage.css";
import Navbar from "./Navbar";
import Section from './Section';
import SignUpButton from './SignUpButton';
import React from "react";
import { useState } from "react";
import { Switch, Route, Router, Link } from "react-router-dom";
const Home = () => {
  const [open, setOpen] = useState(true);

  const clickOpen = () => {
    setOpen(true);
  };

  const clickClose = () => {
    setOpen(false);
  };

  document.title = "Home";

  return (
    <div className={"homepage"}>
      <Navbar
        open={open}
        setOpen={setOpen}
        handleClickOpen={clickOpen}
        handleClose={clickClose}
      />
      <Section />
      <SignUpButton
        open={open}
        setOpen={setOpen}
        clickOpen={clickOpen}
        clickClose={clickClose}
      />
    </div>
  );
};

export default Home;