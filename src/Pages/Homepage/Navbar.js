import './nav.css';
import NavbarList from "./NavbarList";
import {Link} from "react-router-dom";
import React from "react";

const Navbar = ({handleClickOpen,handleClose,open,setOpen}) => {

    const navLogo = 'https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-30.png'

    return(
        <div className={'Nav'}>
            <div className={'NavItems'}>
                <img src={navLogo} alt={'navLogo'}/>
                <NavbarList/>
            </div>
        </div>
    )
}

export default Navbar;
