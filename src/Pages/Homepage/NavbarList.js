import "./NavbarList.css";
import { Button } from "@material-ui/core";
import React from "react";
import SignUpButton from "./SignUpButton";
import { Link } from "react-router-dom";
const NavbarList = ({ clickOpen, clickClose, open, setOpen }) => {
  // const styled = {
  //     color : '#61D5DF',
  //     border: '1px solid #61D5DF',

  // }
  const styled = {
    color: "#61D5DF",
    // background: "#61D5DF",
    marginLeft: "10px",
    // width: '200px',
    // maxHeight: '50px',
  };

  const logOutHandler = () => {
    localStorage.clear();
  };

  return (
    <div className={"Navlist"}>
      <ul className={"Nav__Items"}>
        <li className={"Nav__List"}>ABOUT</li>
        <Link to="/login" style={{ textDecoration: "none" }}>
          <li className={"Nav__List"}>CATALOG</li>
        </Link>

        <li className={"Nav__List"}>PRICING</li>
        <li className={"Nav__List"}>SUPPLIERS</li>
        <li className={"Nav__List"}>HELP CENTER</li>
        <li className={"Nav__List"}>BLOG</li>
        {/* <SignUpButton 
                open={open}
                clickOpen={clickOpen}
                clickClose={clickClose}/> */}
        <Link to="/signup" style={{ textDecoration: "none" }}>
          <Button
            // className={"signup__btn"}
            style={styled}
            style={{ borderColor: "#61D5DF", color: "#61D5DF" }}
            underline="none"
            variant="outlined"
            href="#outlined-buttons"
          >
            Sign up Now{" "}
          </Button>
        </Link>
        <Link to="/login" style={{ textDecoration: "none" }}>
          <Button
            style={styled}
            // variant="outlined"
            underline="none"
            onClick={clickOpen}
          >
            LOGIN
          </Button>
        </Link>
        <Link to="/login" style={{ textDecoration: "none" }}>
          <Button
            style={styled}
            // variant="outlined"
            underline="none"
            onClick={logOutHandler}
          >
            LOG OUT
          </Button>
        </Link>
      </ul>
    </div>
  );
};

export default NavbarList;
