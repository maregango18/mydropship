import './Section.css';

const Section = () => {

    const logo = 'https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg'

    return(
        <div className={'Section'}>
            <div className={'Section__Box'}>
                <div className={'Section__Logo'}>
                    <img src={logo} alt={'logo'}/>
                </div>
                <div className={'Section__Text'}>
                    <h4>
                        WE GOT YOUR SUPPLY CHAIN COVERED
                    </h4>
                    <h4>
                        365 DAYS A YEAR!
                    </h4>
                </div>
            </div>
        </div>
    )
}

export default Section;