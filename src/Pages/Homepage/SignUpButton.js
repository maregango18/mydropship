import {Button} from "@material-ui/core";
import './SignUpButton.css'
import React, {useEffect} from "react";
import {Link} from 'react-router-dom';


const SignUpButton = ({clickOpen,clickClose,open}) => {

    const styled = {
        color : '#fff',
        background: '#61D5DF',
        width: '200px',
        maxHeight: '70px',
    }

    console.log(open)

    return (
        <div className={'SignUpButton'}>
            <Link to="/signup" style={{textDecoration:'none'}}>
            <Button variant="outlined" style={styled}  onClick={clickOpen} >
                SING UP NOW
            </Button>
            </Link>
        </div>
    )
}

export default SignUpButton;