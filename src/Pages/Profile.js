import Logout from "../Components/Logout";
import Navbar from "../Pages/Homepage/Navbar";
import Sidebar from "../Components/Sidebar";
import "../Pages/Profile.css";

const Profile = () => {
  return (
    <div className={"Profile"}>
      <Navbar />
      <Sidebar />

      <h4>Welcome to 365Dropship : This is Profile page</h4>
    </div>
  );
};
export default Profile;
