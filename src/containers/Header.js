import React from "react";
import "./Header.css";
import Button from "../Components/Button";
import Search from "../Components/Search";
import Sort from "../Components/Sort";
import { useDispatch, useSelector } from "react-redux";
import {
  RemoveAllAction,
  SelectAllAction,
} from "../redux/actions/productActions";
// import HelpOutlineIcon from '@material-ui/icons/HelpOutline';

const Header = ({
  query,
  onChange,
  changeQuery,
  getFinalQuery,
  checkedProductsCount,
  numberOfProducts,
  selectAll,
  clearSelectedProducts,
}) => {
  const products = useSelector((state) => state.products.products);
  const dispatch = useDispatch();
  const selectedProducts = useSelector(
    (state) => state.products.selectedProducts
  );

  const clickHandlerSelectAll = () => {
    const AllProducts = products.map((product) => product.id);
    dispatch(SelectAllAction(AllProducts));
  };
  const clickHandlerRemoveAll = () => {
    dispatch(RemoveAllAction());
  };

  return (
    <>
      <header className={"header"}>
        <div className={"header__content"}>
          {/* <Button content={"Select All"} onClick={clickHandler}/> */}
          <div className="header__content">
            <button
              className="header__selectAll"
              onClick={clickHandlerSelectAll}
              style={{
                backgroundColor: "#61d5df",
              }}
            >
              Select All
            </button>
          </div>
          <span className={"header__selection"}>
            selected {selectedProducts.length} out of {products.length} products
          </span>
          <div className="header__content">
            <button
              className="header__selectAll"
              onClick={clickHandlerRemoveAll}
              style={{
                backgroundColor: "#61d5df",
              }}
            >
              Clear
            </button>
          </div>
          {/* <Button content={"Clear"} /> */}
        </div>
        <div className={"header__content"}>
          <Search
            query={query}
            changeQuery={changeQuery}
            getFinalQuery={getFinalQuery}
          />
          <Button
            content={"add to inventory"}
            style={{
              backgroundColor: "#61d5df",
            }}
          />
          <i class="far fa-question-circle header__questionmark"></i>
        </div>
      </header>
      <Sort onChange={onChange} />
    </>
  );
};

export default Header;
