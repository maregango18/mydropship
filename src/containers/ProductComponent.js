import React from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Checkbox from "@material-ui/core/Checkbox";
import "./ProductListing.css";
import {
  RemoveSelectedProduct,
  SelectProductAction,
} from "../redux/actions/productActions";
import { addToCart } from "../Components/API";
import Button from "@material-ui/core/Button";
import { showSuccessSnackbar } from "../redux/actions/SnackbarAction";

const ProductComponent = ({ chooseProduct }) => {
  const products = useSelector((state) => state.products.products);
  const selectedProducts = useSelector(
    (state) => state.products.selectedProducts
  );
  const dispatch = useDispatch();

  const SelectProductHandler = (id) => {
    if (selectedProducts.includes(id)) {
      dispatch(RemoveSelectedProduct(id));
      dispatch(showSuccessSnackbar("Success!"));
    } else {
      dispatch(SelectProductAction(id));
      dispatch(showSuccessSnackbar("Success!"));
    }
  };

  const addCart = (id) => {
    addToCart(id, 1)
      .then((r) => {
        console.log("miha");
        dispatch({ type: "SNACKBAR_SUCCESS", message: "Success" });
      })
      .catch((err) => alert(err));
  };

  const renderList = products.map((product) => {
    const { id, title, image, price, category } = product;

    return (
      <div
        className={
          selectedProducts.includes(id)
            ? "catalog__product--highlighted"
            : "catalog__product"
        }
        key={id}
      >
        <Link to={`/product/${id}`}></Link>
        {/* <input className="checkbox__btn" type="checkbox" ></input> */}
        {/* <Checkbox checked={selectedProducts.includes(id)? true: false} className="checkbox__btn" color="primary" onChange={SelectProductHandler} /> */}
        <div>
          <Checkbox
            checked={selectedProducts.includes(id) ? true : false}
            className="checkbox__btn"
            color="primary"
            style={{
              color: "#61d5df",
            }}
            onChange={() => SelectProductHandler(id)}
          />
          <Button
            style={{
              backgroundColor: "#61d5df",
              width: "180px",
              height: "40px",
              color: "#fff",
              marginLeft: "100px",
            }}
            variant={"contained"}
            onClick={() => addCart(id)}
            endIcon
          >
            Add To Inventory
          </Button>
        </div>
        <div class="catalog__photo" onClick={() => chooseProduct(product)}>
          <img src={product.imageUrl} alt=" " />
        </div>
        <div className="catalog__title"> {product.title}</div>
        <div className="supplier">
          <span className="by"> By: </span> SP-Supplier115
        </div>
        <div class="catalog__prices">
          <div className="cat__price">
            <div>$100</div>
            <span>RRP</span>
          </div>
          <div className="cat__price">
            <div>${product.price}</div>
            <span>COST</span>
          </div>
          <div className="cat__price">
            <div className="profit">10%($10)</div>
            <span>PROFIT</span>
          </div>
        </div>
      </div>
    );
  });
  return <>{renderList}</>;
};

export default ProductComponent;
