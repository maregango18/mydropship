import React, { useState,useEffect, useCallback, useMemo } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { setProducts } from "../redux/actions/productActions";
import ProductComponent from "./ProductComponent";
import { Container } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Aside from '../Components/aside/Aside';
import Header from './Header';
import './ProductListing.css';
import Sidebar from "../Components/Sidebar";
import {addToCart,getProducts} from '../Components/API';
import {sortType} from './SortType';
import {Router}  from 'react-router-dom';
import Modal from '../Components/Modal';
import { getCartAction } from "../redux/actions/CartAction";




const ProductPage = () => {

  // const [MyProducts, setMyProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const chooseProduct = (product) => {
    setSelectedProduct(product);
  }
  const products = useSelector((state) => state.products.products);
  const dispatch = useDispatch();

  
  const query = useSelector(state => state.products.search);
  const sortState = useSelector(state => state.products.sort);
  useEffect( () => {
    getProducts().then(result => {
        let list = (sortType(result.filter((value) => {
            return value.title.toLowerCase().includes(query.toLowerCase())
        }), sortState))
        dispatch(setProducts(list))
    })
}, [query,sortState])
  console.log("Products :", products);

  useEffect(() => {
    dispatch(getCartAction())
  },[dispatch])
  return (
        
        <>
        {selectedProduct && (
          <Modal
            selectedProduct={selectedProduct}
            setSelectedProduct={setSelectedProduct}
          />
        )}
        <Sidebar/>
       <div className="wrapper"> 
       
       <Aside/>
       <div className="Content">
        <Header/>
        <div className="catalog">
          <ProductComponent chooseProduct={chooseProduct} />
          {/* <Grid item key={ProductComponent.id} xs={12} md={6} lg={4}>
            
          </Grid> */}
        </div>
      </div>
    </div>
    </>
   
  );
};

export default ProductPage;