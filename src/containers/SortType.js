export const sortType = (result, sortState) => {
    if (sortState === "asc") {
        return [...result.sort((a, b) => (a.price < b.price) ? 1 : -1)]
    } else if (sortState ==="desc") {
        return [...result.sort((a, b) => (a.price > b.price) ? 1 : -1)]
    } else if (sortState === "profH") {
        return [...result.sort((a, b) => (a.title > b.title) ? 1 : -1)]
    } else if (sortState === "profL") {
        return [...result.sort((a, b) => (a.title < b.title) ? 1 : -1)]
    } else
        return [...result]
    
}
