import { ActionTypes } from "../constants/action-types";

export const setProducts = (products) => {
  return {
    type: ActionTypes.SET_PRODUCTS,
    payload: products,
  };
};

export const sortChangeAction = (data) => {
  return {
    type: ActionTypes.SORT_CHANGE,
    payload:data,
  };
};

export const ProductSearchAction = (data) => {
  return {
    type: ActionTypes.PRODUCT_SEARCH,
    payload:data,
  };
};

export const SelectAllAction = (data) => {
  return {
    type:ActionTypes.SELECT_ALL,
    payload:data,
  };
};

export const RemoveAllAction = (data) => {
  return {
    type:ActionTypes.REMOVE_ALL,
  }
}

export const SelectProductAction = (product) => {
  return {
    type: ActionTypes.SELECT_PRODUCT,
    payload: product,
  };
};
export const RemoveSelectedProduct = (id) => {
  return {
    type: ActionTypes.REMOVE_SELECTED_PRODUCT,
    payload:id,
  };
};

