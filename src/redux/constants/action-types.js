export const ActionTypes = {
  SET_PRODUCTS: "SET_PRODUCTS",
  SELECT_PRODUCT: "SELECT_PRODUCT",
  REMOVE_SELECTED_PRODUCT: "REMOVE_SELECTED_PRODUCT",
  SORT_CHANGE: "SORT_CHANGE",
  PRODUCT_SEARCH: "PRODUCT_SEARCH",
  SELECT_ALL: "SELECT_ALL",
  REMOVE_ALL: "REMOVE_ALL",
  GET_CART: "GET_CART",
  SNACKBAR_SUCCESS: "SNACKBAR_SUCCESS",
  SNACKBAR_CLEAR: "SNACKBAR_CLEAR",
};
