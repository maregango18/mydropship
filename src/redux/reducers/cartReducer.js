import { ActionTypes } from "../constants/action-types";

const initialState = {
    cartList:[]
}

const cartReducer = (state= initialState, action) => {
    switch (action.type) {
        case ActionTypes.GET_CART:
            return {...state,cartList:action.payload.cartProducts}
        default:
            return {...state};   
    }
        
}
export default cartReducer;