import { combineReducers } from "redux";
import { productsReducer } from "./productsReducer";
import { uiReducer } from "./uiReducer";
import cartReducer from "./cartReducer";

const reducers = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  snackbar: uiReducer,
});
export default reducers;
