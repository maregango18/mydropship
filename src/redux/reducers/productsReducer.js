import { ActionTypes } from "../constants/action-types";
const initialState = {
  products: [],
  fetchedProducts:[],
  sort: 'default',
  search: '',
  selectedProducts: [],

};

export const productsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_PRODUCTS:
      return { ...state, products: payload };
    case ActionTypes.SORT_CHANGE:
      return {...state,sort:payload};
    case ActionTypes.PRODUCT_SEARCH:
      return {...state,search:payload};
    case ActionTypes.SELECT_ALL:
      return {...state,selectedProducts:payload};
    case ActionTypes.REMOVE_ALL:
      return {...state,selectedProducts:[]};
    case ActionTypes.SELECT_PRODUCT:
      return {...state,selectedProducts: [...state.selectedProducts, payload ]};
    case ActionTypes.REMOVE_SELECTED_PRODUCT:
      return {...state,selectedProducts:[ ...state.selectedProducts.filter(id => id !== payload)]};
    default:
      return state;
  }
};



// export const selectedProductsReducer = (state = {}, { type, payload }) => {
//   console.log(type);
//   switch (type) {
//     case ActionTypes.SELECTED_PRODUCT:
//       return { ...state, ...payload };
//     case ActionTypes.REMOVE_SELECTED_PRODUCT:
//       return {};
//     default:
//       return state;
//   }
// };